/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2555.h
**
** Description:
**     header file for tas2555.c
**
** =============================================================================
*/

#ifndef TAS2555_H_
#define TAS2555_H_

#include <stdint.h>

#define CONFIGURATION 0
#define CONFIGURATION_CALIBRATION 2

extern void tas2555_load_configuration(int nConfiguration);
extern void tas2555_load_calibration(int nCalibration);
extern uint32_t tas2555_coeff_read(uint8_t book, uint8_t page, uint8_t reg);
extern void tas2555_coeff_write(uint8_t book, uint8_t page, uint8_t reg, uint32_t data);
extern void tas2555_save_cal(double re, double f0, double q, double t_cal,
                             uint32_t rms_pow, uint32_t t_limit, double test_delta_tv, uint32_t result,
                             char * pFileName);
extern void tas2555_open_bin(char * pFileName, unsigned int nConfiguration);
extern void tas2555_close_bin(void);

//extern void tas2555_init(void);

#endif /* TAS2555_H_ */
