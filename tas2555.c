/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2555.c
**
** Description:
**     functions for configurating TAS2555 Android device
**
** =============================================================================
*/

#include "tas2555.h"
#include "system.h"

#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#define TILOAD_NODE "/dev/tiload_node"

int gTILoad = 0;
int gBinFile = 0;

#define MAX_BIN_SIZE 2048
char gpBin[MAX_BIN_SIZE];
unsigned int gnBinIndex = 0;
unsigned int gnBinBlockIndex = 0;

void tas2555_mixer_command(char *pCommand, int nData)
{
	char *pMixer[] = {AUDIO_MIXER, pCommand, "0", NULL};
	char *pEnv[] = {NULL};

	char pData[16];

	printf("TAS2555 mixer command %s = %d.\n\r", pCommand, nData);

	sprintf(pData, "%d", nData);
	pMixer[2] = pData;
	
	if (0 == (fork()))
	{
		if (execve(AUDIO_MIXER, pMixer, pEnv) == -1)
		{
			printf("factorytest: Can't find mixer. Please install %s. \n\r", AUDIO_MIXER);
			exit(-1);
		}
	}

	sys_delay(500);
}

void tas2555_load_configuration(int nConfiguration)
{
	tas2555_mixer_command("Configuration", nConfiguration);
}

void tas2555_load_calibration(int nCalibration)
{
	tas2555_mixer_command("Calibration", nCalibration);
}

void tas2555_check_node(void)
{
	if (gTILoad) return;

	gTILoad = open(TILOAD_NODE, O_RDWR);

	if (gTILoad < 0)
	{
		printf("factorytest: Can't find tiload. Please create %s. \n\r", TILOAD_NODE);
		exit(-1);
	}

	printf("factorytest: %s handle = %d\n\r", TILOAD_NODE, gTILoad);
}

uint32_t tas2555_coeff_read(uint8_t book, uint8_t page, uint8_t reg)
{
	unsigned char pPageZero[] = {0x00, 0x00};
	unsigned char pBook[] = {0x7F, book};
	unsigned char pPage[] = {0x00, page};
	unsigned char pData[] = {reg, 0x00, 0x00, 0x00, 0x00};

	tas2555_check_node();

	write(gTILoad, pPageZero, 2);
	write(gTILoad, pBook, 2);
	write(gTILoad, pPage, 2);
	read(gTILoad, pData, 4);

	return ((pData[0] << 24) | (pData[1] << 16) | (pData[2] << 8) | (pData[3]));
}

void tas2555_coeff_write(uint8_t book, uint8_t page, uint8_t reg, uint32_t data)
{
	unsigned int nByte;

	if (gBinFile) // if the bin file is open, write the coefficients to the bin file
	{
		for (nByte = 0; nByte < 4; nByte++)
		{
			gpBin[gnBinIndex++] = book;
			gpBin[gnBinIndex++] = page;
			gpBin[gnBinIndex++] = reg + nByte;
			gpBin[gnBinIndex++] = (data >> ((3 - nByte) * 8)) & 0xFF;
		}
	}
	else
	{
		unsigned char pPageZero[] = {0x00, 0x00};
		unsigned char pBook[] = {0x7F, book};
		unsigned char pPage[] = {0x00, page};
		unsigned char pData[] = {reg, (data & 0xFF000000) >> 24, (data & 0x00FF0000) >> 16, (data & 0x0000FF00) >> 8, data & 0x000000FF};

		tas2555_check_node();

		write(gTILoad, pPageZero, 2);
		write(gTILoad, pBook, 2);
		write(gTILoad, pPage, 2);
		write(gTILoad, pData, 5);

		pBook[1] = 0x8C;
		pPage[1] = 0x19;

		pData[0] = 0x7C;
		pData[1] = 0x00;
		pData[2] = 0x00;
		pData[3] = 0x00;
		pData[4] = 0x01;

		write(gTILoad, pPageZero, 2);
		write(gTILoad, pBook, 2);
		write(gTILoad, pPage, 2);
		write(gTILoad, pData, 5);
	}
}

void tas2555_save_cal(double re, double f0, double q, double t_cal,
                             uint32_t rms_pow, uint32_t t_limit, double test_delta_tv, uint32_t result,
                             char * pFileName)
{
	printf("TAS2555 calibration values:\n\r");
	if (result) printf("Calibration did not pass. Speaker out of bounds. Check result parameter.\n\r");
	printf("    Re = %1.2f Ohm\n\r", re);
	printf("    F0 = %3.0f Hz\n\r", f0);
	printf("    Q  = %1.3f\n\r", q);
	printf("    t0 = %2.2f C\n\r\n\r", t_cal);
	printf("    rms_pow       = 0x%08X\n\r", rms_pow);
	printf("    t_limit       = 0x%08X\n\r", t_limit);
	printf("    test_delta_tv = %2.2f C\n\r", test_delta_tv);
	printf("    result        = 0x%08X\n\r", result);


	FILE *pFile = fopen(pFileName, "w+");

	if (result) fprintf(pFile, "Calibration did not pass. Speaker out of bounds. Check result parameter.\n\r");
	fprintf(pFile, "Re = %1.2f\n\r", re);
	fprintf(pFile, "F0 = %3.0f\n\r", f0);
	fprintf(pFile, "Q  = %1.3f\n\r", q);
	fprintf(pFile, "t0 = %2.2f\n\r\n\r", t_cal);
	fprintf(pFile, "rms_pow       = 0x%08X\n\r", rms_pow);
	fprintf(pFile, "t_limit       = 0x%08X\n\r", t_limit);
	fprintf(pFile, "test_delta_tv = %2.2f C\n\r", test_delta_tv);
	fprintf(pFile, "result        = 0x%08X\n\r", result);

	fclose(pFile);
}

void tas2555_open_bin(char * pFileName, unsigned int nConfiguration)
{
	gBinFile = open(pFileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH);

	memset(gpBin, 0, MAX_BIN_SIZE);

	gpBin[3] = '2';
	gpBin[2] = '5';
	gpBin[1] = '5';
	gpBin[0] = '5';

	strcpy(&gpBin[24], "Calibration Data File");
	strcpy(&gpBin[24 + 64], "Calibration Data File for TAS2555");

	gnBinIndex += 24 + 64 + strlen(&gpBin[24 + 64]) + 1;
	
	gnBinIndex +=
		4 +	//device family index
		4 +	//device index
		2 +	//num PLL index
		0 +	//array PLL index
		2 +	//num programs index
		0 +	//array programs index
		2 + 	//num configurations index
		0;	//array configurations index
	
	gpBin[gnBinIndex++] = 0x00;
	gpBin[gnBinIndex++] = 0x01; // one calibration data block

	strcpy(&gpBin[gnBinIndex], "Calibration Data");
	gnBinIndex += 64;
	strcpy(&gpBin[gnBinIndex], "Calibration Data for TAS2555");
	gnBinIndex += strlen(&gpBin[gnBinIndex]) + 1;
	gpBin[gnBinIndex++] = 0x00; // compatible program = smart amp (index 0)
	gpBin[gnBinIndex++] = nConfiguration; // compatible configuration
	
	gpBin[gnBinIndex++] = 0x00;
	gpBin[gnBinIndex++] = 0x00;
	gpBin[gnBinIndex++] = 0x00;
	gpBin[gnBinIndex++] = 0x0A; // block type = 0x0A (calibration)
	gnBinBlockIndex = gnBinIndex;

	gnBinIndex += 4; // number of commands index		
}

void tas2555_close_bin(void)
{
	unsigned int nCommands;
	unsigned char pCommit[] =
	{
		0x00, 0x04, 0x85, 0x00,
		0x8C, 0x19, 0x7C, 0x00,
		0x00, 0x00, 0x01, 0x00
	};	
	
	// write the commit sequence
	memcpy(&gpBin[gnBinIndex], pCommit, 12);
	gnBinIndex += 12;

	nCommands = ((gnBinIndex - gnBinBlockIndex) / 4) - 1;

	// write number of commands for calibration block
	gpBin[gnBinBlockIndex++] = (nCommands & 0xFF000000) >> 24;
	gpBin[gnBinBlockIndex++] = (nCommands & 0x00FF0000) >> 16;
	gpBin[gnBinBlockIndex++] = (nCommands & 0x0000FF00) >> 8;
	gpBin[gnBinBlockIndex++] = (nCommands & 0x000000FF);

	// write bin file size
	gpBin[4] = (gnBinIndex & 0xFF000000) >> 24;
	gpBin[5] = (gnBinIndex & 0x00FF0000) >> 16;
	gpBin[6] = (gnBinIndex & 0x0000FF00) >> 8;
	gpBin[7] = (gnBinIndex & 0x000000FF);

	write(gBinFile, gpBin, gnBinIndex);
	close(gBinFile);

	gBinFile = 0;
}

//void tas2555_init(void)
//{
//}
