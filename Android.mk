LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES += \
      factorytest.c \
      system.c \
      tas2555.c \
      tas2555_ftc.c	  

LOCAL_C_INCLUDES += ${LOCAL_PATH}/

LOCAL_CFLAGS += -Wno-unused-parameter

LOCAL_SHARED_LIBRARIES += libm

LOCAL_STATIC_LIBRARIES += libftc

LOCAL_MODULE := factorytest

include $(BUILD_EXECUTABLE)

#include $(CLEAR_VARS)

include $(call all-makefiles-under,$(LOCAL_PATH))
