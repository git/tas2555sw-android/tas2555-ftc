/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     factorytest.c
**
** Description:
**     main program for TAS2555 factory test 
**
** =============================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

#include "tas2555_ftc.h"

// ================================================================ Definitions
// Obtained from Speaker Manufacturer
#define SPK_T_MAX            100    // Speaker Maximum Temperature (C)
#define SPK_RE_TOL_PER       10     // Re +/- tolerance (%)
#define SPK_RE_ALPHA         0.0039 // Temperature coefficient alpha (1/K)

// Obtained from PurePath Console 3 (PPC3)
#define PPC3_RE0             7.41   // Re0 (ohm)
#define PPC3_RTV             46.5   // Rtv (K/W)
#define PPC3_RTM             78.2   // Rtm (K/W)
#define PPC3_RTVA            2460   // Rtva (K/W)
#define PPC3_SYSGAIN         9.35   // System Gain (V/FS)
#define PPC3_DEV_NONLIN_PER  1.5    // Device Non-linearity (%)
#define PPC3_DELTA_T_LIMIT   80     // Delta Thermal Limit (C)

// Pass/Fail Limits
#define RE_HI                8.2
#define RE_LO                6.9
#define F0_HI                990.0
#define F0_LO                850.0
#define Q_HI                 3.5
#define Q_LO                 2.5
#define T_HI                 25
#define T_LO                 15

#define RESULT_PASS          0x00000000
#define RE_FAIL_HI           0x00000001
#define RE_FAIL_LO           0x00000010
#define F0_FAIL_HI           0x00000100
#define F0_FAIL_LO           0x00001000
#define Q_FAIL_HI            0x00010000
#define Q_FAIL_LO            0x00100000
#define T_FAIL_HI            0x01000000
#define T_FAIL_LO            0x10000000

#define TEST_DELTA_T         20
#define FS_RATE              48000  // TAS2555 Sample Rate
#define FTC_BYPASS           0      // Bypasses this tool if = 1

void ExitWithHint(char *pHint)
{
	printf("factorytest: invalid command line: %s\n\r\n\r", pHint);

	printf("usage: factorytest [-t temperature] [-c configuration file] [-l load calibration] [-v verbose]\n\r");
	exit(-1);
}

void InitFTCC(TFTCConfiguration *pFTCC)
{
	pFTCC->nCalibrationTime = 4000;
	pFTCC->nVerificationTime = 4000;

	pFTCC->bVerbose = false;
	pFTCC->bLoadCalibration = false;
	pFTCC->bCalDeltaTPresent = false;
	pFTCC->bFTCBypass = FTC_BYPASS;
	pFTCC->nTestDeltaT = TEST_DELTA_T;
	pFTCC->nConfiguration = 0;
	pFTCC->nConfigurationCalibration = 1;
	
	pFTCC->nSpkTMax = SPK_T_MAX;
	pFTCC->nSpkReTolPer = SPK_RE_TOL_PER;
	pFTCC->nSpkReAlpha = SPK_RE_ALPHA;
	
	pFTCC->nPPC3_Re0 = PPC3_RE0;
	pFTCC->nPPC3_RTV = PPC3_RTV;
	pFTCC->nPPC3_RTM = PPC3_RTM;
	pFTCC->nPPC3_RTVA = PPC3_RTVA;
	pFTCC->nPPC3_SysGain = PPC3_SYSGAIN;
	pFTCC->nPPC3_DevNonlinPer = PPC3_DEV_NONLIN_PER;
	pFTCC->nPPC3_DeltaTLimit = PPC3_DELTA_T_LIMIT;
	pFTCC->nFSRate = FS_RATE;

	pFTCC->nNFS = 0.0001;
	pFTCC->nSCTH = 0.99999;
	
	pFTCC->nReHi = RE_HI;
	pFTCC->nReLo = RE_LO;
	pFTCC->nF0Hi = F0_HI;
	pFTCC->nF0Lo = F0_LO;
	pFTCC->nQHi = Q_HI;
	pFTCC->nQLo = Q_LO;
	pFTCC->nTHi = T_HI;
	pFTCC->nTLo = T_LO;
}

unsigned int SkipCharacter(char *pData, char cCharacter, unsigned int nSize)
{
	unsigned int nRIndex;
	unsigned int nWIndex = 0;

	for (nRIndex = 0; nRIndex < nSize; nRIndex++)
		if (pData[nRIndex] != cCharacter) pData[nWIndex++] = pData[nRIndex];

	return nWIndex;
}

unsigned int RemoveComments(char *pData, char cCharacter, unsigned int nSize)
{
	unsigned int nRIndex;
	unsigned int nWIndex = 0;

	for (nRIndex = 0; nRIndex < nSize; nRIndex++)
	{
		if (pData[nRIndex] == cCharacter)
			while ((nRIndex < nSize) && (pData[nRIndex] != '\r')) nRIndex++;
		pData[nWIndex++] = pData[nRIndex];
	}

	return nWIndex;
}

void ReadValue(TFTCConfiguration *pFTCC, char *pLine, char *pValue)
{
	if (!strcmp(pLine, "CALIBRATION_TIME")) {pFTCC->nCalibrationTime = atoi(pValue); return;};
	if (!strcmp(pLine, "VERIFICATION_TIME")) {pFTCC->nVerificationTime = atoi(pValue); return;};
	if (!strcmp(pLine, "CAL_DELTA_T")) {
		pFTCC->nCalDeltaT = atof(pValue); 
		pFTCC->bCalDeltaTPresent = true;
		return;
	};
	if (!strcmp(pLine, "TEST_DELTA_T")) {pFTCC->nTestDeltaT = atof(pValue); return;};
	if (!strcmp(pLine, "CONFIGURATION")) {pFTCC->nConfiguration = atoi(pValue); return;};
	if (!strcmp(pLine, "CONFIGURATION_CALIBRATION")) {pFTCC->nConfigurationCalibration = atoi(pValue); return;};
	if (!strcmp(pLine, "SPK_T_MAX")) {pFTCC->nSpkTMax = atof(pValue); return;};
	if (!strcmp(pLine, "SPK_RE_TOL_PER")) {pFTCC->nSpkReTolPer = atof(pValue); return;};
	if (!strcmp(pLine, "SPK_RE_ALPHA")) {pFTCC->nSpkReAlpha = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_RE0")) {pFTCC->nPPC3_Re0 = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_RTV")) {pFTCC->nPPC3_RTV = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_RTM")) {pFTCC->nPPC3_RTM = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_RTVA")) {pFTCC->nPPC3_RTVA = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_SYSGAIN")) {pFTCC->nPPC3_SysGain = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_DEV_NONLIN_PER")) {pFTCC->nPPC3_DevNonlinPer = atof(pValue); return;};
	if (!strcmp(pLine, "PPC3_DELTA_T_LIMIT")) {pFTCC->nPPC3_DeltaTLimit = atof(pValue); return;};
	if (!strcmp(pLine, "FS_RATE")) {pFTCC->nFSRate = atoi(pValue); return;};
	if (!strcmp(pLine, "RE_HI")) {pFTCC->nReHi = atof(pValue); return;};
	if (!strcmp(pLine, "RE_LO")) {pFTCC->nReLo = atof(pValue); return;};
	if (!strcmp(pLine, "F0_HI")) {pFTCC->nF0Hi = atof(pValue); return;};
	if (!strcmp(pLine, "F0_LO")) {pFTCC->nF0Lo = atof(pValue); return;};
	if (!strcmp(pLine, "Q_HI")) {pFTCC->nQHi = atof(pValue); return;};
	if (!strcmp(pLine, "Q_LO")) {pFTCC->nQLo = atof(pValue); return;};
	if (!strcmp(pLine, "T_HI")) {pFTCC->nTHi = atof(pValue); return;};
	if (!strcmp(pLine, "T_LO")) {pFTCC->nTLo = atof(pValue); return;};
	if (!strcmp(pLine, "NFS")) {pFTCC->nNFS = atof(pValue); return;};
	if (!strcmp(pLine, "SCTH")) {pFTCC->nSCTH = atof(pValue); return;};
}

void ftcc_print(TFTCConfiguration *pFTCC)
{
	printf("factorytest configuration: \n\r");

	printf("  CALIBRATION_TIME   = %d\n\r", pFTCC->nCalibrationTime);
	printf("  VERIFICATION_TIME  = %d\n\r\n\r", pFTCC->nVerificationTime);
	printf("  TEST_DELTA_T = %2.2f\n\r\n\r", pFTCC->nTestDeltaT);
	printf("  CAL_DELTA_T = %2.2f\n\r\n\r", pFTCC->nCalDeltaT);
	printf("  CONFIGURATION             = %d\n\r", pFTCC->nConfiguration);
	printf("  CONFIGURATION_CALIBRATION = %d\n\r\n\r", pFTCC->nConfigurationCalibration);
	
	printf("  SPK_T_MAX      = %2.2f\n\r", pFTCC->nSpkTMax);
	printf("  SPK_RE_TOL_PER = %2.2f\n\r", pFTCC->nSpkReTolPer);
	printf("  SPK_RE_ALPHA   = %2.4f\n\r\n\r", pFTCC->nSpkReAlpha);
	
	printf("  PPC3_RE0            = %2.2f\n\r", pFTCC->nPPC3_Re0);
	printf("  PPC3_RTV            = %2.2f\n\r", pFTCC->nPPC3_RTV);
	printf("  PPC3_RTM            = %2.2f\n\r", pFTCC->nPPC3_RTM);
	printf("  PPC3_RTVA           = %2.2f\n\r", pFTCC->nPPC3_RTVA);
	printf("  PPC3_SYSGAIN        = %2.2f\n\r", pFTCC->nPPC3_SysGain);
	printf("  PPC3_DEV_NONLIN_PER = %2.2f\n\r", pFTCC->nPPC3_DevNonlinPer);
	printf("  PPC3_DELTA_T_LIMIT  = %2.2f\n\r\n\r", pFTCC->nPPC3_DeltaTLimit);
	
	printf("  NFS = %1.4f\n\r\n\r", pFTCC->nNFS);

	printf("  FS_RATE = %d\n\r\n\r", pFTCC->nFSRate);

	printf("  RE_HI = %2.3f\n\r", pFTCC->nReHi);
	printf("  RE_LO = %2.3f\n\r", pFTCC->nReLo);
	printf("  F0_HI = %2.2f\n\r", pFTCC->nF0Hi);
	printf("  F0_LO = %2.2f\n\r", pFTCC->nF0Lo);
	printf("  Q_HI  = %2.3f\n\r", pFTCC->nQHi);
	printf("  Q_LO  = %2.3f\n\r", pFTCC->nQLo);
	printf("  T_HI  = %2.2f\n\r", pFTCC->nTHi);
	printf("  T_LO  = %2.2f\n\r", pFTCC->nTLo);
}

int ftcc_parse(TFTCConfiguration *pFTCC, char *pData, unsigned int nSize)
{
	unsigned int nRIndex = 0;
	char *pLine;
	char *pEqual;
	double nTest;

	nSize = SkipCharacter(pData, ' ', nSize);
	nSize = RemoveComments(pData, ';', nSize);

	pData[nSize] = 0;

	pLine = strtok(pData, "\n\r");

//	printf("ftcc_parse: pData = %s\n\r", pData);

//	printf("ftcc_parse: size = %d, pLine = 0x%08x\n\r", nSize, pLine);
	while (pLine)
	{
//		printf("ftcc_parse: pLine = 0x%08x\n\r", pLine);
		if (pLine[0])
		{
			printf("Line = %s\n\r", pLine);
			pEqual = strstr(pLine, "=");
			if ((pEqual) && (strlen(pEqual) > 1))
			{
				pEqual[0] = 0;
				ReadValue(pFTCC, pLine, pEqual + 1);
				pEqual[0] = '=';
			}
		}
		pLine = strtok(NULL, "\n\r");
	}
	
	return 0;
}

void LoadFTCC(char *pFileName, TFTCConfiguration *pFTCC)
{
	struct stat st;
	char *pData;
	int nFile;
	
//	printf("LoadFTCC: %s\n\r", pFileName);

	if (stat(pFileName, &st) < 0) ExitWithHint("configuration file doesn't exist");

	pData = malloc(st.st_size);
	if (!pData) ExitWithHint("cannot allocate memory for configuation file");

    nFile = open(pFileName, O_RDONLY);
	if (nFile < 0) ExitWithHint("cannot open configuration file"); 

	read(nFile, pData, st.st_size);
	close(nFile);

	ftcc_parse(pFTCC, pData, st.st_size); 

	free(pData);
}

int main(int argc, char *argv[])
{
	double nTemp = 20.0;
	int nArg = 1;
	bool bValidArg;
	char pHint[256];
	TFTCConfiguration sFTCC;

	printf("\n\rTI TAS2555 factory test calibration sequence V1.0\n\r");

	InitFTCC(&sFTCC);
//	printf("argc = %d\n\r", argc);
	while (nArg < argc)
	{
//		printf("argv[%d] = %s: ", nArg, argv[nArg]);
		bValidArg = false;
		if (!strcmp(argv[nArg], "-t"))
		{
			printf("nArg = %d, argc = %d\n\r", nArg, argc);
			if (argc <= (nArg + 1)) ExitWithHint("temperature parameter is missing");
			nTemp = atof(argv[nArg + 1]);
			if(sFTCC.bCalDeltaTPresent == false){
				sFTCC.nCalDeltaT = sFTCC.nSpkTMax - nTemp;
			}
			nArg++;
			bValidArg = true;
		}

		if (!strcmp(argv[nArg], "-c"))
		{
//			printf("found configuration file argument\n\r");
			if (argc <= (nArg + 1)) ExitWithHint("configuration file name is missing");
			LoadFTCC(argv[nArg + 1], &sFTCC);
			if(sFTCC.bCalDeltaTPresent == false){
				sFTCC.nCalDeltaT = sFTCC.nSpkTMax - nTemp;
			}
			nArg++;
			bValidArg = true;
		}

		if (!strcmp(argv[nArg], "-v"))
		{
			sFTCC.bVerbose = true;
			bValidArg = true;
		}

		if (!strcmp(argv[nArg], "-l"))
		{
			sFTCC.bLoadCalibration = true;
			bValidArg = true;
		}

		if (!bValidArg)
		{
			sprintf(pHint, "don't know argument %s", argv[nArg]);
			ExitWithHint(pHint);
		}
		nArg++;
	}

	if (sFTCC.bVerbose) 
	{
		printf("\n\rambient temperature = %2.1f\n\r", nTemp);
		ftcc_print(&sFTCC);
	}
	tas2555_ftc(nTemp, &sFTCC);
	return 0;
}
