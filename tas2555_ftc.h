/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2555_ftc.h
**
** Description:
**     header file for tas2555_ftc.c
**
** =============================================================================
*/

#ifndef TAS2555_FTC_H_
#define TAS2555_FTC_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
	bool bVerbose;
	bool bLoadCalibration;

	unsigned int nCalibrationTime;
	unsigned int nVerificationTime;

	double nNFS;
	double nSCTH;

	bool bFTCBypass;
	double nCalDeltaT;
	bool bCalDeltaTPresent;
	double nTestDeltaT;
	unsigned int nConfiguration;
	unsigned int nConfigurationCalibration;
	
	double nSpkTMax;
	double nSpkReTolPer;
	double nSpkReAlpha;
	
	double nPPC3_Re0;
	double nPPC3_RTV;
	double nPPC3_RTM;
	double nPPC3_RTVA;
	double nPPC3_SysGain;
	double nPPC3_DevNonlinPer;
	double nPPC3_DeltaTLimit;
	unsigned int nFSRate;
	
	double nReHi;
	double nReLo;
	double nF0Hi;
	double nF0Lo;
	double nQHi;
	double nQLo;
	double nTHi;
	double nTLo;
} TFTCConfiguration;

uint32_t tas2555_ftc(double t_cal, TFTCConfiguration *pFTCC);

void set_scth(double nSCTH);
void set_nfs(double nNFS);
void set_re(double re_ppc3, double re, double alpha);
void set_temp_cal(uint32_t prm_pow, uint32_t prm_tlimit);
double get_re(double re_ppc3);
double get_f0(int32_t fs);
double get_q(int32_t fs);
uint32_t calc_prm_pow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain);
uint32_t calc_prm_tlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA);
double calc_safe_prm_tlimit(double prm_tlimit_safe, double alpha, double nDevNonlinPer, double nSpkReTolPer);
static double coeff_fixed_to_float(uint32_t coeff);


#endif /* TAS2555_FTC_H_ */
