/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** This program is free software; you can redistribute it and/or modify it under
** the terms of the GNU General Public License as published by the Free Software 
** Foundation; version 2.
**
** This program is distributed in the hope that it will be useful, but WITHOUT
** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
** FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along with
** this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
** Street, Fifth Floor, Boston, MA 02110-1301, USA.
**
** File:
**     tas2555_ftc.c
**
** Description:
**     factory test program for TAS2555 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "tas2555.h"        // TAS2555 Driver
#include "tas2555_ftc.h"    // TAS2555 Factory Test and Calibration Tool

uint32_t check_spk_bounds(double re, double f0, double q, double test_delta_tv);

#define RESULT_PASS          0x00000000
#define RE_FAIL_HI           0x00000001
#define RE_FAIL_LO           0x00000010
#define F0_FAIL_HI           0x00000100
#define F0_FAIL_LO           0x00001000
#define Q_FAIL_HI            0x00010000
#define Q_FAIL_LO            0x00100000
#define T_FAIL_HI            0x01000000
#define T_FAIL_LO            0x10000000

#define PI                   3.14159

TFTCConfiguration *gpFTCC;

// -----------------------------------------------------------------------------
// tas2555_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
uint32_t tas2555_ftc(double t_cal, TFTCConfiguration *pFTCC)
{
    double re = pFTCC->nPPC3_Re0;           // Default Re
    double f0 = 0;                  // Default f0
    double q = 0;                   // Default Q
    double test_delta_tv = 0;       // Verification Delta Tv
    uint32_t prm_pow = 0;           // Total RMS power coefficient
    uint32_t prm_tlimit = 0;        // Delta temperature limit coefficient
    uint32_t result = RESULT_PASS;
    pid_t nPlaybackProcess;

    gpFTCC = pFTCC;

    // STEP 1: Load TAS2555 calibration configuration
    tas2555_load_configuration(gpFTCC->nConfigurationCalibration);

    // STEP 2: Play calibration signal
    nPlaybackProcess = sys_play_wav("TAS2555_cal_m28dB.wav", "loop");

    // STEP 3: Re-program Re for worst case
    re = gpFTCC->nPPC3_Re0 * (1 - 2*gpFTCC->nSpkReTolPer/100.0);
    prm_pow = calc_prm_pow (re, 
		gpFTCC->nCalDeltaT, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA, 
		gpFTCC->nPPC3_SysGain);
    prm_tlimit = calc_prm_tlimit(gpFTCC->nCalDeltaT, 
		gpFTCC->nSpkReAlpha, 
		gpFTCC->nPPC3_DevNonlinPer, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA);	
    set_re(gpFTCC->nPPC3_Re0, re, gpFTCC->nSpkReAlpha);
	set_temp_cal(prm_pow, prm_tlimit);

    set_nfs(gpFTCC->nNFS);
    set_scth(gpFTCC->nSCTH);

    // STEP 4: Wait for algorithm to converge
    sys_delay(gpFTCC->nCalibrationTime); // Delay 4 seconds

    // STEP 5: Get actual Re, f0 and Q from TAS2555
    re = get_re(gpFTCC->nPPC3_Re0);
    f0 = get_f0(gpFTCC->nFSRate);
    q = get_q(gpFTCC->nFSRate);
 //   t_cal = sys_get_ambient_temp(); // Set t_cal to ambient temperature //use command line parameter instead


    // STEP 6: Verify Speaker at gpFTCC->nTestDeltaT
    prm_pow = calc_prm_pow (re, 
		gpFTCC->nTestDeltaT, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA, 
		gpFTCC->nPPC3_SysGain);

    prm_tlimit = calc_prm_tlimit(gpFTCC->nTestDeltaT, 
		gpFTCC->nSpkReAlpha, 
		gpFTCC->nPPC3_DevNonlinPer, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA);
    set_re(gpFTCC->nPPC3_Re0, re, gpFTCC->nSpkReAlpha);
    set_temp_cal(prm_pow, prm_tlimit);
    sys_stop_wav(nPlaybackProcess);

    nPlaybackProcess = sys_play_wav("TAS2555_verify_m06.wav", "loop");
    sys_delay(gpFTCC->nVerificationTime); // Delay 4 seconds
    test_delta_tv = (get_re(gpFTCC->nPPC3_Re0)/re - 1) / gpFTCC->nSpkReAlpha;
    result = check_spk_bounds(re, f0, q, test_delta_tv);

    // STEP 7: Set temperature limit to target TMAX
    prm_pow = calc_prm_pow (re, 
		gpFTCC->nSpkTMax - t_cal, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA, 
		gpFTCC->nPPC3_SysGain);
    prm_tlimit = calc_prm_tlimit(gpFTCC->nSpkTMax - t_cal, 
		gpFTCC->nSpkReAlpha, 
		gpFTCC->nPPC3_DevNonlinPer, 
		gpFTCC->nPPC3_RTV, 
		gpFTCC->nPPC3_RTM, 
		gpFTCC->nPPC3_RTVA);
    set_re(gpFTCC->nPPC3_Re0, re, gpFTCC->nSpkReAlpha);
    set_temp_cal(prm_pow, prm_tlimit);
    sys_stop_wav(nPlaybackProcess);

    // STEP 8: Save Re, f0, Q and Cal Temp into a file
    tas2555_save_cal(re, 
		f0, q, t_cal, 
		prm_pow, prm_tlimit, 
		test_delta_tv, result, "tas2555_cal.txt");

    // STEP 9: Save .bin file for TAS2555 driver
    if (!result)
    {
        tas2555_open_bin("tas2555_cal.bin", gpFTCC->nConfiguration);
        set_re(gpFTCC->nPPC3_Re0, re, gpFTCC->nSpkReAlpha);
        set_temp_cal(prm_pow, prm_tlimit);
        tas2555_close_bin();

	if (gpFTCC->bLoadCalibration) tas2555_load_calibration(0xFF);
    }    

    return result;
}

// -----------------------------------------------------------------------------
// check_spk_bounds
// -----------------------------------------------------------------------------
// Description:
//      Checks if speaker paramters are within bounds.
// -----------------------------------------------------------------------------
uint32_t check_spk_bounds(double re, double f0, double q, double test_delta_tv)
{
    uint32_t result = RESULT_PASS;

    if (gpFTCC->bVerbose)
    {
	printf("TEST_DELTA_TV = %2.1f\n\r", test_delta_tv);
    }

    if(re>gpFTCC->nReHi)
        result |= RE_FAIL_HI;
    if(re<gpFTCC->nReLo)
        result |= RE_FAIL_LO;
    if(f0>gpFTCC->nF0Hi)
        result |= F0_FAIL_HI;
    if(f0<gpFTCC->nF0Lo)
        result |= F0_FAIL_LO;
    if(q>gpFTCC->nQHi)
        result |= Q_FAIL_HI;
    if(q<gpFTCC->nQLo)
        result |= Q_FAIL_LO;
    if(test_delta_tv>gpFTCC->nTHi)
        result |= T_FAIL_HI;
    if(test_delta_tv<gpFTCC->nTLo)
        result |= T_FAIL_LO;

    return result;
}
